from elg import QuartService
from elg.model import TextRequest, TextsResponse
from transformers import BartForConditionalGeneration, BartTokenizer


class ELGModel(QuartService):
    def __init__(self, name: str):
        self.model = BartForConditionalGeneration.from_pretrained(
            "./distilbart-multi-combine-wiki-news"
        )
        self.tok = BartTokenizer.from_pretrained("./distilbart-multi-combine-wiki-news")
        super().__init__(name)

    async def process_text(self, request: TextRequest):
        batch = self.tok.encode_plus(
            request.content, max_length=1024, return_tensors="pt", truncation=True
        )
        request.params = request.params if request.params else {}
        outputs = self.model.generate(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            early_stopping=bool(request.params.get("early_stopping", True)),
            max_length=int(request.params.get("max_length", 142)),
            min_length=int(request.params.get("min_length", 56)),
            num_beams=int(request.params.get("num_beams", 4)),
            temperature=float(request.params.get("temperature", 1.0)),
            repetition_penalty=float(request.params.get("repetition_penalty", 1.2)),
            length_penalty=float(request.params.get("length_penalty", 2.0)),
            no_repeat_ngram_size=int(request.params.get("no_repeat_ngram_size", 3)),
            encoder_no_repeat_ngram_size=int(
                request.params.get("encoder_no_repeat_ngram_size", 0)
            ),
        )
        result = self.tok.decode(
            outputs[0], skip_special_tokens=True, clean_up_tokenization_spaces=False
        )
        return TextsResponse(texts=[{"content": result}])


s = ELGModel("")
app = s.app
